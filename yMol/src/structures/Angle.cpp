/*
 * Angle.cpp
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#include "Angle.h"

Angle::Angle() {
  b0 = 0;
  kb = 0;
  func = 0;
}

std::shared_ptr<Atom>  Angle::getA() const {
  return a;
}

std::shared_ptr<Atom>  Angle::getB() const {
  return b;
}

double Angle::getB0() const {
  return b0;
}

void Angle::setB0(double b0) {
  this->b0 = b0;
}

std::shared_ptr<Atom>  Angle::getC() const {
  return c;
}

int Angle::getFunc() const {
  return func;
}

void Angle::setFunc(int func) {
  this->func = func;
}

double Angle::getKb() const {
  return kb;
}

void Angle::setAtoms( std::shared_ptr<Atom>  a,
                      std::shared_ptr<Atom>  b,
                      std::shared_ptr<Atom>  c) {
  this->a = a;
  this->b = b;
  this->c = c;
}

void Angle::setKb(double kb) {
  this->kb = kb;
}

Angle::~Angle() {
  // TODO Auto-generated destructor stub
}

std::shared_ptr<Angle> Angle::create() {
  std::shared_ptr<Angle> pangl(new Angle());
  return pangl;
}
