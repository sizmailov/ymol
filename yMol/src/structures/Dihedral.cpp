/*
 * Torsion.cpp
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#include "Dihedral.h"

Diherdal::Diherdal() {
  func = 0;
  c0 = c1 =c2 = c3 = c4 = c5 = 0.0;
}

std::shared_ptr<Atom> Diherdal::getA() const {
  return a;
}

std::shared_ptr<Atom> Diherdal::getB() const {
  return b;
}

std::shared_ptr<Atom> Diherdal::getC() const {
  return c;
}



double Diherdal::getC0() const {
  return c0;
}
double Diherdal::getC1() const {
  return c1;
}

double Diherdal::getC2() const {
  return c2;
}


double Diherdal::getC3() const {
  return c3;
}


double Diherdal::getC4() const {
  return c4;
}


double Diherdal::getC5() const {
  return c5;
}


std::shared_ptr<Atom> Diherdal::getD() const {
  return d;
}


int Diherdal::getFunc() const {
  return func;
}

void Diherdal::setFunc(int func) {
  this->func = func;
}

void Diherdal::setAtoms(
    std::shared_ptr<Atom> a,
    std::shared_ptr<Atom> b,
    std::shared_ptr<Atom> c,
    std::shared_ptr<Atom> d) {
  this->a = a;
  this->b = b;
  this->c = c;
  this->d = d;
}

void Diherdal::setC_(double c0, double c1, double c2, double c3, double c4, double c5) {
  this->c0 = c0;
  this->c1 = c1;
  this->c2 = c2;
  this->c3 = c3;
  this->c4 = c4;
  this->c5 = c5;
}



Diherdal::~Diherdal() {
  // TODO Auto-generated destructor stub
}



std::shared_ptr<Diherdal> Diherdal::create() {
  std::shared_ptr<Diherdal> pTor(new Diherdal());
  return pTor;
}
