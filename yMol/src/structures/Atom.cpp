/*
 * Atom.cpp
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#include "Atom.h"
Atom::Atom() :
			   r(0.0,0.0,0.0)
			   {

  residue_serial=0;
  serial = 0;
  type = 0;

  residue_name = "";
  chain_id= "";
  name = "";
}
Atom::Atom(const Point &pos, int atom_type) :
		r(pos){

  residue_serial=0;
  type = atom_type;
  serial = 0;

  residue_name = "";
  chain_id= "";
  name = "";
}

Atom::~Atom() {
	// TODO Auto-generated destructor stub
}


int Atom::getSerial() const {
  return serial;
}

void Atom::setSerial(int serial) {
  this->serial = serial;
}

int Atom::getType() const {
  return type;
}

void Atom::setType(int type) {
  this->type = type;
}


const std::string& Atom::getChainId() const {
  return chain_id;
}

void Atom::setChainId(const std::string& chainId) {
  chain_id = chainId;
}

const std::string& Atom::getName() const {
  return name;
}

void Atom::setName(const std::string& name) {
  this->name = name;
}

const std::string& Atom::getResidueName() const {
  return residue_name;
}

void Atom::setResidueName(const std::string& residueName) {
  residue_name = residueName;
}



int Atom::getResidueSerial() const {
  return residue_serial;
}

double Atom::getCharge() const {
  return charge;
}

void Atom::setCharge(double charge) {
  this->charge = charge;
}

void Atom::setResidueSerial(int residueSerial) {
  residue_serial = residueSerial;
}


