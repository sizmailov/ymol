/*
 * Angle.h
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#ifndef ANGLE_H_
#define ANGLE_H_

#include <vector>
#include "Atom.h"


class Angle : boost::noncopyable{
  private:
    std::shared_ptr<Atom> a;
    std::shared_ptr<Atom> b;
    std::shared_ptr<Atom> c;

    double b0;
    double kb;
    int func;
  public:
    virtual ~Angle();
    
    std::shared_ptr<Atom> getA() const;
    std::shared_ptr<Atom> getB() const;
    std::shared_ptr<Atom> getC() const;
    
    double getB0() const;
    double getKb() const;
    int getFunc() const;
    
    
    void setAtoms(std::shared_ptr<Atom> a,
                  std::shared_ptr<Atom> b,
                  std::shared_ptr<Atom> c);
    
    void setB0(double b0);
    void setKb(double kb);
    void setFunc(int func);
    
    

    static std::shared_ptr<Angle> create();
  private:
    Angle();
};

#endif /* ANGLE_H_ */
