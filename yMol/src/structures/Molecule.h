/*
 * Molecule.h
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#ifndef MOLECULE_H_
#define MOLECULE_H_

#include <vector>
#include <string>

#include "Atom.h"
#include "PotentialParameters.h"


class Molecule {
protected:
	std::vector <Atom>  atoms;
	std::string name;
public:
	Molecule();
	virtual ~Molecule();

	void addAtom(const Atom & a);
	void addAtoms(const std::vector<Atom> &va);

	std::string getName() const;
	void setName(const std::string & name);
	virtual void printAtoms(std::ostream &out,const PotentialParameters & prm)const;

	std::size_t size();
};


#endif /* MOLECULE_H_ */
