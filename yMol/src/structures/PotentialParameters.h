/*
 * PotentialParameters.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef POTENTIALPARAMETERS_H_
#define POTENTIALPARAMETERS_H_

#include "ResidueTopology.h"
#include <map>
class PotentialParameters  {

    ResidueTopology base;
    std::vector<ResidueTopology> residues;
    mutable std::map<std::string, int> typeID;

  public:
    PotentialParameters(const std::string & parameter_filename);
    ResidueTopology getResidueTopology(const std::string & res_name) const;
    AtomType getAtomTypeByAmberName(const std::string & name)const;
    void print(std::ostream & out) const;

    int getAtomTypeID(const std::string & type_name) const;
    AtomType  getAtomType(const std::string & type_name)const;
    AtomType  getAtomType(int type_id)const;

    BondType getBondType(int type1, int type2) const;
    AngleType getAngleType(int type1, int type2,int type3) const;
    TorsionType getTorsionType(int type1, int type2,int type3,int type4) const;
    TorsionType getSpecificTorsionType(const std::string & residueName, const std::string & name1
        , const std::string & name2
        , const std::string & name3
        , const std::string & name4) const;

    virtual ~PotentialParameters();

  private:
    void makeMap();
    bool matchesType(int t1, int t2) const;
    bool matchesName(const std::string &name1 , const std::string & name2) const;
};

#endif /* POTENTIALPARAMETERS_H_ */
