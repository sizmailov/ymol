/*
 * Bond.cpp
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#include "Bond.h"

Bond::Bond() {
  // TODO Auto-generated constructor stub
  func = 0;
  kb = b0 = 0.0;
}

Bond::~Bond() {
  // TODO Auto-generated destructor stub
}

double Bond::getB0() const {
  return b0;
}

void Bond::setB0(double b0) {
  this->b0 = b0;
}

int Bond::getFunc() const {
  return func;
}

void Bond::setFunc(int func) {
  this->func = func;
}

double Bond::getKb() const {
  return kb;
}

void Bond::setAtoms(std::shared_ptr<Atom> a,
    std::shared_ptr<Atom> b) {
  this->a = a;
  this->b = b;
}

std::shared_ptr<Atom> Bond::getA() const {
  return a;
}

std::shared_ptr<Atom> Bond::getB() const {
  return b;
}

void Bond::setKb(double kb) {
  this->kb = kb;
}

std::shared_ptr<Bond> Bond::create() {
  std::shared_ptr<Bond> pbond(new Bond());
  return pbond;
}
