/*
 * Protein.h
 *
 *  Created on: 1 Jun 2014
 *      Author: sergei
 */

#ifndef PROTEIN_H_
#define PROTEIN_H_

#include "Molecule.h"
#include "Residue.h"
#include "BondType.h"
#include "Bond.h"
#include "Angle.h"
#include "Torsion.h"
class PotentialParameters;

class Protein: public Molecule {
    std::vector <Residue> residues;
    std::vector <Bond> bonds;
    std::vector <Angle> angles;
    std::vector <Torsion> torsions;
    std::vector < std::vector<int> > neigh;
  public:
    Protein();
    virtual ~Protein();

    void detectResidues();
    void printResidues(std::ostream & out);

    void correctAtomNames(const PotentialParameters & prm);
    void setTerminalResidues();

    void setBonds(const PotentialParameters & prm );
    void setAngles(const PotentialParameters & prm);
    void setTorsions(const PotentialParameters & prm);
    void setSpecificTorsions(const PotentialParameters & prm);

    void printAngles(std::ostream & out );
    void printTorsions(std::ostream & out);
};

bool readPdbFile(const std::string& filename, Protein & prot);

#endif /* PROTEIN_H_ */
