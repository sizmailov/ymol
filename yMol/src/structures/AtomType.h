/*
 * AtomType.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef ATOMTYPE_H_
#define ATOMTYPE_H_
#include <string>

class AtomType {
  private:

    /* order as in GROMACS files */
    std::string name;
    std::string amber_name;
    double mass;
    double charge;
    std::string pType;
    double sigma;
    double epsilon;
    int serial;

  public:
    AtomType();
    AtomType(std::string name, std::string bond_type, double mass,
        double charge, std::string pType, double sigma, double epsion, int serial);

    virtual ~AtomType();
    const std::string& getAmberName() const;
    void setAmberName(const std::string& bondType);
    double getCharge() const;
    void setCharge(double charge);
    double getEpsilon() const;
    void setEpsilon(double epsilon);
    double getMass() const;
    void setMass(double mass);
    const std::string& getName() const;
    void setName(const std::string& name);
    const std::string& getPType() const;
    void setPType(const std::string& type);
    double getSigma() const;
    void setSigma(double sigma);
    int getSerial() const;
    void setSerial(int serial);
};

#endif /* ATOMTYPE_H_ */
