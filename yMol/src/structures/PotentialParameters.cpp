/*
 * PotentialParameters.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "PotentialParameters.h"
#include <fstream>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <cstdlib>

PotentialParameters::PotentialParameters(const std::string & parameter_filename) {
  std::ifstream fin;
  fin.open(parameter_filename.c_str(),std::ifstream::in);

  if (!fin.good())
  {
    std::cerr<<"ERROR: Could not open file '"<< parameter_filename<<"'"<< std::endl;
    return;
  }

  std::vector <std::string> sections =
            {"defaults","atomtypes","bondtypes","angletypes","dihedraltypes","bondedtypes"};

  enum scanPos{ defaults,atomtypes,bondtypes,angletypes,dihedraltypes,bondedtypes,
                BaseLine,Residue,Atom,Bond,Angle,Torsion,
                undefined};

  scanPos spos = BaseLine;

  boost::regex    section("\\[ (?<SectionName>.*) \\]\\s*(;.*)?");
  boost::regex subsection(" \\[ (?<SubsectionName>.*) \\]\\s*(;.*)?");// note space before `\\[`
  boost::regex comment_line("\\s*;.*"); // comment char = `;`


  boost::smatch what;


  while (fin.good())
  {
    std::string line;
    std::getline(fin,line);

    if (boost::regex_match(line,what,comment_line)) // skip comment lines
    {
      continue;
    }

    if (spos==Atom)
    {
      if (residues.back().addAtomType(line))
      {
        continue;
      }
    }

    if (spos==Bond)
    {
      if (
        residues.back().addBondType(line)){
        continue;
      }
    }

    if (spos==Angle)
    {
      if (residues.back().addAngleType(line)){
        continue;
      }
    }

    if (spos==Torsion)
    {

      if (residues.back().addTorsionType(line))
      {
        continue;
      }
    }
    if (spos == atomtypes)
    {
      if (base.addAtomType(line))
        continue;
    }

    if (spos == bondtypes)
    {
     if (base.addBondType(line))
       continue;
    }

    if (spos == angletypes)
    {
     if (base.addAngleType(line))
       continue;
    }
    if (spos == dihedraltypes)
    {
     if (base.addTorsionType(line))
       continue;
    }
    if (spos == bondedtypes)
    {
      std::cerr << "WARNING: Cound not hold section bonded types" << std::endl;

    }
    if (spos == defaults)
    {
      std::cerr << "WARNING: Cound not hold section bonded types" << std::endl;

    }



    if (boost::regex_match(line,what,subsection)) // try to update subsection
    {
        std::string subsection = what["SubsectionName"];
        if (subsection=="atoms" && spos==Residue)
        {
          spos = Atom;
          continue;
        }else
        if (subsection=="bonds" && spos==Atom)
        {
          spos = Bond;
          continue;
        }else
        if (subsection=="angles" && spos==Bond)
        {
          spos = Angle;
          continue;
        }else
        if (subsection=="dihedrals" && (spos==Bond || spos == Angle))
        {
          spos = Torsion;
          continue;
        }else
        {
          spos = undefined;
          std::cerr << "WARNING: unknown subsection :"<< subsection << std::endl;
        }
    }


    if (boost::regex_match(line,what,section) ) // try to update section
    {
      std::string section_name = what["SectionName"];
      if (std::find(sections.begin(), sections.end(), section_name)!=sections.end())//in sections
      {
//        defaults,atomtypes,bondtypes,angletypes,dihedraltypes,bondedtypes,
        if (section_name == "defaults"     ) {spos = defaults; continue;}
        if (section_name == "atomtypes"    ) {spos = atomtypes;continue;}
        if (section_name == "bondtypes"    ) {spos = bondtypes;continue;}
        if (section_name == "angletypes"   ) {spos = angletypes;continue;}
        if (section_name == "dihedraltypes") {spos = dihedraltypes;continue;}
        if (section_name == "bondedtypes"  ) {spos = bondedtypes;continue;}
      }else // residue enterence
      {
        spos = Residue;
//        std::cout << section_name << std::endl;
        ResidueTopology rt;
        rt.setName(section_name);
        residues.push_back(rt);
        continue;
      }
    }

    if (spos==undefined) continue;


    std::cerr <<"WARNING: NO pattern for line '" << line << "'" << std::endl;

  }



  makeMap();


  std::map < std::string , int > atomName_to_typeID;
  auto ats = base.getAtomTypes();
  for (auto it = ats.begin();it!=ats.end();++it)
  {
    atomName_to_typeID[it->getName()] = typeID.at(it->getAmberName());
  }
  atomName_to_typeID["X"]=-1; // matches any name
  base.setBondTypes(atomName_to_typeID);

}

void PotentialParameters::print(std::ostream& out) const {

  out << " ---   BASE  --- " << std::endl;
  base.print(out);
  out << " --- RESIDUES  --- " << std::endl;
  for (auto it=residues.begin();it!=residues.end();++it)
  {
    std::cout <<  it->getName() << " :" << std::endl;
    it->print(std::cout);
  }
}

PotentialParameters::~PotentialParameters() {

}

ResidueTopology PotentialParameters::getResidueTopology(
    const std::string& res_name) const {
  for (auto it = residues.begin(); it != residues.end(); ++it)

  {
    if (it->getName()== res_name)
    {
      return *it;
    }
  }
  std::cerr << "NO SUCH RESIDUE: "<< res_name << std::endl;
  exit(1);
}

AtomType PotentialParameters::getAtomTypeByAmberName(const std::string& name) const
{
  return base.getAtomTypeByAmberName(name);
}

BondType PotentialParameters::getBondType(int typeI, int typeJ) const
{
  auto btypes= base.getBondTypes();
  for (auto it = btypes.begin();it!=btypes.end();++it)
  {
    if (it->getTypeI() == typeI &&  it->getTypeJ() == typeJ )
    {
      return *it;
    }
    if (it->getTypeI()  == typeJ && it->getTypeJ() == typeI )
    {
      return *it;
    }
  }

  return *btypes.begin();
}

AngleType PotentialParameters::getAngleType(int type1, int type2,
    int type3) const {
  auto atypes= base.getAngleTypes();
    for (auto it = atypes.begin();it!=atypes.end();++it)
    {
      if (it->getTypeI() == type1 &&  it->getTypeJ() == type2 &&  it->getTypeK() == type3)
      {
        return *it;
      }

      if (it->getTypeI() == type3 &&  it->getTypeJ() == type2 &&  it->getTypeK() == type1)
      {
        return *it;
      }
    }

    return *atypes.begin();
}

TorsionType PotentialParameters::getTorsionType(int type1, int type2,
    int type3, int type4) const {
  auto torsions= base.getTorsionTypes();

  for (auto it = torsions.begin();it!=torsions.end();++it)
  {

    if (matchesType(it->getTypeI(), type1) &&
        matchesType(it->getTypeJ(), type2) &&
        matchesType(it->getTypeK(), type3) &&
        matchesType(it->getTypeL(), type4) )
    {
          return *it;
    }

    if (matchesType(it->getTypeI(), type4) &&
        matchesType(it->getTypeJ(), type3) &&
        matchesType(it->getTypeK(), type2) &&
        matchesType(it->getTypeL(), type1) )
    {
          return *it;
    }
  }
  return *torsions.begin();
}

void PotentialParameters::makeMap() {

  typeID.clear();
  const auto atom_types(base.getAtomTypes());
  for (auto it = atom_types.begin();it!=atom_types.end();++it)
  {
    int id = typeID.size();
    typeID[it->getAmberName()]= id;
//    std::cout << "typeID["<< it->getAmberName() <<"]="<<typeID[it->getAmberName()] << std::endl;
  }
//  std::cout << "typeID[\"\"]="<<typeID[""] << std::endl;
}

int PotentialParameters::getAtomTypeID(const std::string& type_name) const
{
  if (typeID.count(type_name)==0) return 0;
  int id = typeID.at(type_name);
  return id;
}

AtomType PotentialParameters::getAtomType(const std::string& type_name) const {
  return base.getAtomTypes()[getAtomTypeID(type_name)];
}

AtomType PotentialParameters::getAtomType(int type_id) const {
  const auto atom_types(base.getAtomTypes());
  if (type_id<0 || type_id >=atom_types.size()) return atom_types[0];
  return atom_types[type_id];
}

bool PotentialParameters::matchesType(int t1, int t2) const
{
  if (t1<0 || t2 < 0) return true;
  if (t1==t2) return true;
  return false;
}

TorsionType PotentialParameters::getSpecificTorsionType(
    const std::string& residueName, const std::string & name1
    , const std::string & name2
    , const std::string & name3
    , const std::string & name4) const {

  const ResidueTopology & rt = getResidueTopology(residueName);
  const std::vector<TorsionType> & torsions= rt.getTorsionTypes();
//  std::cerr <<"Torsions" << torsions.size() << std::endl;

   for (auto it = torsions.begin();it!=torsions.end();++it)
   {

     if (matchesName(it->getNameI(), name1) &&
         matchesName(it->getNameJ(), name2) &&
         matchesName(it->getNameK(), name3) &&
         matchesName(it->getNameL(), name4) )
     {
           return *it;
     }

     if (matchesName(it->getNameI(), name4) &&
         matchesName(it->getNameJ(), name3) &&
         matchesName(it->getNameK(), name2) &&
         matchesName(it->getNameL(), name1) )
     {
           return *it;
     }
   }
   auto it = torsions.begin();
//   std::cerr << "NO TORSION "  << name1<<" "<< name2<<" "<<name3<<" "<<name4 <<" in "<< residueName <<"   "<< __LINE__ << __FILE__ << std::endl;
   return *torsions.begin();
}

bool PotentialParameters::matchesName(const std::string &name1 , const std::string & name2) const
{
  if (name1=="X") return true;
  if (name2=="X") return true;
  if (name1==name2) return true;
  return false;
}
