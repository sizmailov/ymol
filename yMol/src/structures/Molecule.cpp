/*
 * Molecule.cpp
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#include "Molecule.h"

Molecule::Molecule() : atoms(0), name("") {

}

Molecule::~Molecule() {

}

void Molecule::addAtom(const Atom& a) {
	atoms.push_back(a);
}

void Molecule::addAtoms(const std::vector<Atom>& va) {
	atoms.insert(atoms.end(),va.begin(),va.end());
}

std::string Molecule::getName() const {
	return name;
}

void Molecule::setName(const std::string& name) {
	this->name = name;
}

void Molecule::printAtoms(std::ostream& out,const PotentialParameters & prm) const{
  out <<atoms.size()<<"\t # Atoms" << std::endl;
  out <<  "serial" << "\t";
  out << "name"  << "\t";
  out << "r_name"  << "\t";
  out << "X"  << "\t";
  out << "Y" << "\t";
  out << "Z" << "\t";
  out << "charge" << "\t";
  out << "mass" << "\t";
  out << "type" << "\t";
  out <<  std::endl;

  for (auto it = atoms.begin();it!=atoms.end();++it)
  {
    AtomType atype = prm.getAtomType(it->getType());
    out << it->getSerial() << "\t";
    out << it->getName() << "\t";
    out << it->getResidueName() << "\t";
    out << it->r.x << "\t";
    out << it->r.y << "\t";
    out << it->r.z << "\t";
    out << it->getCharge() << "\t";
    out << atype.getMass() << "\t";
    out << it->getType() << "\t";
    out <<  std::endl;
  }
}

std::size_t Molecule::size() {
	return atoms.size();
}

