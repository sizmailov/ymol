/*
 * BondType.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef BONDTYPE_H_
#define BONDTYPE_H_
#include <string>

class BondType {
  private:
    std::string name_i;
    std::string name_j;
    int type_i;
    int type_j;
    int func;
    double b0;
    double kb;

  public:
    BondType();
    BondType(std::string name_i, std::string name_j, int func, double b0, double kb);
    virtual ~BondType();
    double getB0() const;
    void setB0(double b0);
    int getFunc() const;
    void setFunc(int func);
    double getKb() const;
    void setKb(double kb);
    const std::string& getNameI() const;
    void setNameI(const std::string& nameI);
    const std::string& getNameJ() const;
    void setNameJ(const std::string& nameJ);
    int getTypeI() const;
    void setTypeI(int typeI);
    int getTypeJ() const;
    void setTypeJ(int typeJ);
};

#endif /* BONDTYPE_H_ */
