/*
 * Residue.h
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#ifndef RESIDUE_H_
#define RESIDUE_H_

#include "AtomSelection.h"

class Residue {
	int serial;


//	std::string name1;//  1-letter residue code (  E,   C,   P, etc.)
	std::string name3;//  3-letter residue code (GLU, CYS, PRO, etc.)

	AtomSelection atoms;

public:
	Residue();
	virtual ~Residue();


	  void addAtom( std::vector <Atom> :: iterator it );

    const std::string& getName() const;
    void setName(const std::string& name3);
    int getSerial() const;
    void setSerial(int serial);
    std::vector<Atom>::iterator getAtom(const std::string & name);

    const AtomSelection & getAtoms() const;

};

#endif /* RESIDUE_H_ */
