/*
 * AngleType.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "AngleType.h"

AngleType::AngleType(std::string name_i,std::string name_j,std::string name_k,int func, double th0,double cth):
name_i(name_i),
 name_j(name_j),
 name_k(name_k),
 func(func),
 th0(th0),
 cth(0)
{
  // TODO Auto-generated constructor stub

}

double AngleType::getCth() const {
  return cth;
}

void AngleType::setCth(double cth) {
  this->cth = cth;
}

int AngleType::getFunc() const {
  return func;
}

void AngleType::setFunc(int func) {
  this->func = func;
}

const std::string& AngleType::getNameI() const {
  return name_i;
}

void AngleType::setNameI(const std::string& nameI) {
  name_i = nameI;
}

const std::string& AngleType::getNameJ() const {
  return name_j;
}

void AngleType::setNameJ(const std::string& nameJ) {
  name_j = nameJ;
}

const std::string& AngleType::getNameK() const {
  return name_k;
}

void AngleType::setNameK(const std::string& nameK) {
  name_k = nameK;
}

double AngleType::getTh0() const {
  return th0;
}

AngleType::AngleType() {
}

int AngleType::getTypeI() const {
  return type_i;
}

void AngleType::setTypeI(int typeI) {
  type_i = typeI;
}

int AngleType::getTypeJ() const {
  return type_j;
}

void AngleType::setTypeJ(int typeJ) {
  type_j = typeJ;
}

int AngleType::getTypeK() const {
  return type_k;
}

void AngleType::setTypeK(int typeK) {
  type_k = typeK;
}

void AngleType::setTh0(double th0) {
  this->th0 = th0;
}

AngleType::~AngleType() {
  // TODO Auto-generated destructor stub
}

