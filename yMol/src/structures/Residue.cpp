/*
 * Residue.cpp
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#include "Residue.h"
#include <iostream>

Residue::Residue() {
	serial = 0;
	// TODO Auto-generated constructor stub
}

Residue::~Residue() {
	// TODO Auto-generated destructor stub
}


const std::string& Residue::getName() const {
  return name3;
}

void Residue::setName(const std::string& name3) {
  this->name3 = name3;
}

int Residue::getSerial() const {
  return serial;
}

void Residue::setSerial(int serial) {
  this->serial = serial;
}

void Residue::addAtom(std::vector<Atom>::iterator it) {
  atoms.addAtom(it);
}

const AtomSelection & Residue::getAtoms() const {
  return atoms;
}

std::vector<Atom>::iterator Residue::getAtom(const std::string& name) {
  for (std::size_t i=0;i<atoms.size();i++)
  {
    if (atoms[i]->getName() == name )
    {
      return atoms[i];
    }
  }
  std::cerr << "No Atom intstance '" << name << "' " << " in  residue " << getName() << std::endl;

}
