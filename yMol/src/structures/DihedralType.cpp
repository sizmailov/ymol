/*
 * TorsionType.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "TorsionType.h"


TorsionType::TorsionType() :
name_i(""),
name_j(""),
name_k(""),
name_l(""),
func(0),
phase(0),
kd(0),
pn(0){
c0 = c1 =c2 = c3 = c4 = c5 = 0.0;
type_i = type_j = type_k = type_l = 0;
}
TorsionType::TorsionType(std::string name_i, std::string name_j, std::string name_k, std::string name_l, int func, double phase, double kd, double pn):
  name_i(name_i),
  name_j(name_j),
  name_k(name_k),
  name_l(name_l),
  func(func),
  phase(phase),
  kd(kd),
  pn(pn){
  c0 = c1 =c2 = c3 = c4 = c5 = 0.0;

}

int TorsionType::getFunc() const {
  return func;
}

void TorsionType::setFunc(int func) {
  this->func = func;
}

double TorsionType::getKd() const {
  return kd;
}

void TorsionType::setKd(double kd) {
  this->kd = kd;
}

const std::string& TorsionType::getNameI() const {
  return name_i;
}

void TorsionType::setNameI(const std::string& nameI) {
  name_i = nameI;
}

const std::string& TorsionType::getNameJ() const {
  return name_j;
}

void TorsionType::setNameJ(const std::string& nameJ) {
  name_j = nameJ;
}

const std::string& TorsionType::getNameK() const {
  return name_k;
}

void TorsionType::setNameK(const std::string& nameK) {
  name_k = nameK;
}

const std::string& TorsionType::getNameL() const {
  return name_l;
}

void TorsionType::setNameL(const std::string& nameL) {
  name_l = nameL;
}

double TorsionType::getPhase() const {
  return phase;
}

void TorsionType::setPhase(double phase) {
  this->phase = phase;
}

int TorsionType::getPn() const {
  return pn;
}

void TorsionType::setPn(int pn) {
  this->pn = pn;
}

TorsionType::~TorsionType() {
}

int TorsionType::getTypeI() const {
  return type_i;
}

void TorsionType::setTypeI(int typeI) {
  type_i = typeI;
}

int TorsionType::getTypeJ() const {
  return type_j;
}

void TorsionType::setTypeJ(int typeJ) {
  type_j = typeJ;
}

int TorsionType::getTypeK() const {
  return type_k;
}

void TorsionType::setTypeK(int typeK) {
  type_k = typeK;
}

int TorsionType::getTypeL() const {
  return type_l;
}

double TorsionType::getC0() const {
  return c0;
}

double TorsionType::getC1() const {
  return c1;
}

double TorsionType::getC2() const {
  return c2;
}

double TorsionType::getC3() const {
  return c3;
}

double TorsionType::getC4() const {
  return c4;
}

double TorsionType::getC5() const {
  return c5;
}

void TorsionType::setTypeL(int typeL) {
  type_l = typeL;
}
