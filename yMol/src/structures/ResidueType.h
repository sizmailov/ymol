/*
 * ResidueTopology.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef RESIDUETOPOLOGY_H_
#define RESIDUETOPOLOGY_H_

#include <vector>


#include "AtomType.h"
#include "BondType.h"
#include "AngleType.h"
#include "TorsionType.h"
#include <map>


class ResidueTopology {
private:
	std::string name;

protected:
	std::vector < AtomType > atom_types;
	std::vector < BondType > bond_types;
	std::vector < AngleType > angle_types;
	std::vector < TorsionType > torsion_types;
public:
	ResidueTopology();

	/* functions returns true in success transform @input string into Type */
	bool addAtomType(const std::string &input);
	bool addBondType(const std::string &input);
	bool addAngleType(const std::string &input);
	bool addTorsionType(const std::string &input);

	void addAtomType(const AtomType & type);
	void addBondType(const BondType & type);
  void addAngleType(const AngleType & type);
	void addTorsionType(const TorsionType& type);

	AtomType getAtomType(const std::string &name) const;
	AtomType getAtomTypeByAmberName(const std::string &name) const;

	const std::vector<AtomType> & getAtomTypes () const;
	const std::vector<BondType> & getBondTypes() const;
  const std::vector<AngleType> & getAngleTypes() const;
  const std::vector<TorsionType> & getTorsionTypes() const;



	virtual ~ResidueTopology();

	void setBondTypes(const std::map< std::string , int > & name_to_typeID);

  const std::string& getName() const;
  void setName(const std::string& name);
  void printAtomTypes(std::ostream& out) const;
  void printBondTypes(std::ostream& out) const;
  void printAngleTypes(std::ostream& out)const;
  void print(std::ostream & out ) const;
  void printTorsions(std::ostream& out) const;

private:
  static const std::string double_pattern;
  static const std::string name_pattern;

};

#endif /* RESIDUETOPOLOGY_H_ */
