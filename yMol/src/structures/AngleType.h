/*
 * AngleType.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef ANGLETYPE_H_
#define ANGLETYPE_H_

#include <string>

class AngleType {
    std::string name_i;
    std::string name_j;
    std::string name_k;

    int type_i;
    int type_j;
    int type_k;

    int func;
    double th0;
    double cth;
  public:
    AngleType();
    AngleType(std::string name_i,std::string name_j,std::string name_k,int func, double th0,double cth);
    virtual ~AngleType();
    double getCth() const;
    void setCth(double cth);
    int getFunc() const;
    void setFunc(int func);
    const std::string& getNameI() const;
    void setNameI(const std::string& nameI);
    const std::string& getNameJ() const;
    void setNameJ(const std::string& nameJ);
    const std::string& getNameK() const;
    void setNameK(const std::string& nameK);
    double getTh0() const;
    void setTh0(double th0);
    int getTypeI() const;
    void setTypeI(int typeI);
    int getTypeJ() const;
    void setTypeJ(int typeJ);
    int getTypeK() const;
    void setTypeK(int typeK);
};

#endif /* ANGLETYPE_H_ */
