/*
 * Torsion.h
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#ifndef TORSION_H_
#define TORSION_H_

#include "Atom.h"
#include "../utils/Logger.h"


#include <vector>

class Diherdal : boost::noncopyable {
    std::shared_ptr<Atom> a;
    std::shared_ptr<Atom> b;
    std::shared_ptr<Atom> c;
    std::shared_ptr<Atom> d;


    int func;
    double c0,c1,c2,c3,c4,c5;

  public:
    virtual ~Diherdal();

    void setAtoms(std::shared_ptr<Atom> a,
                  std::shared_ptr<Atom> b,
                  std::shared_ptr<Atom> c,
                  std::shared_ptr<Atom> d);

    void setFunc(int func);
    void setC_(double c0, double c1, double c2, double c3, double c4, double c5);
    
    
    
    std::shared_ptr<Atom> getA() const;
    std::shared_ptr<Atom> getB() const;
    std::shared_ptr<Atom> getC() const;
    std::shared_ptr<Atom> getD() const;

    int getFunc() const;
    double getC0() const;
    double getC1() const;
    double getC2() const;
    double getC3() const;
    double getC4() const;
    double getC5() const;


    static std::shared_ptr<Diherdal> create();

  private:

    Diherdal();


};

#endif /* TORSION_H_ */
