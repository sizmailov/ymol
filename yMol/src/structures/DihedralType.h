/*
 * TorsionType.h
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#ifndef TORSIONTYPE_H_
#define TORSIONTYPE_H_

#include <string>

/*
 *
 *  Describes proper dihedral angles in molecule
 *
 *                     * l
 *                    /
 *      j            /
 *        * --------*
 *       /           k
 *      /
 *     * i
 */

class TorsionType {
    std::string name_i;
    std::string name_j;
    std::string name_k;
    std::string name_l;


    int type_i;
    int type_j;
    int type_k;
    int type_l;



    int func;
    double phase;
    double kd;
    int  pn;
    double c0,c1,c2,c3,c4,c5;

  public:
    TorsionType();
    TorsionType(std::string name_i, std::string name_j, std::string name_k,
        std::string name_l, int func, double phase, double kd, double pn);
    virtual ~TorsionType();
    int getFunc() const;
    void setFunc(int func);
    double getKd() const;
    void setKd(double kd);
    const std::string& getNameI() const;
    void setNameI(const std::string& nameI);
    const std::string& getNameJ() const;
    void setNameJ(const std::string& nameJ);
    const std::string& getNameK() const;
    void setNameK(const std::string& nameK);
    const std::string& getNameL() const;
    void setNameL(const std::string& nameL);
    double getPhase() const;
    void setPhase(double phase);
    int  getPn() const;
    void setPn(int pn);


    void setC(double c0, double c1, double c2, double c3, double c4, double c5) {
      this->c0 = c0;
      this->c1 = c1;
      this->c2 = c2;
      this->c3 = c3;
      this->c4 = c4;
      this->c5 = c5;
    }

    int getTypeI() const;
    void setTypeI(int typeI);
    int getTypeJ() const;
    void setTypeJ(int typeJ);
    int getTypeK() const;
    void setTypeK(int typeK);
    int getTypeL() const;
    void setTypeL(int typeL);
    double getC0() const;
    double getC1() const;
    double getC2() const;
    double getC3() const;
    double getC4() const;
    double getC5() const;
};

#endif /* TORSIONTYPE_H_ */
