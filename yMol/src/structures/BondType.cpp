/*
 * BondType.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "BondType.h"

BondType::BondType(std::string name_i, std::string name_j, int func, double b0, double kb):
  name_i(name_i),
  name_j(name_j),
  func(func),
  b0(b0),
  kb(kb)
{

}

double BondType::getB0() const {
  return b0;
}

void BondType::setB0(double b0) {
  this->b0 = b0;
}

int BondType::getFunc() const {
  return func;
}

void BondType::setFunc(int func) {
  this->func = func;
}

double BondType::getKb() const {
  return kb;
}

void BondType::setKb(double kb) {
  this->kb = kb;
}

const std::string& BondType::getNameI() const {
  return name_i;
}

void BondType::setNameI(const std::string& nameI) {
  name_i = nameI;
}

const std::string& BondType::getNameJ() const {
  return name_j;
}

BondType::BondType() {
  name_i="";
  name_j="";
  kb = 0.0;
  b0 = 0.0;
  func = 0;
}

int BondType::getTypeI() const {
  return type_i;
}

void BondType::setTypeI(int typeI) {
  type_i = typeI;
}

int BondType::getTypeJ() const {
  return type_j;
}

void BondType::setTypeJ(int typeJ) {
  type_j = typeJ;
}

void BondType::setNameJ(const std::string& nameJ) {
  name_j = nameJ;
}

BondType::~BondType() {
  // TODO Auto-generated destructor stub
}

