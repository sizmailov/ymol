/*
 * ResidueTopology.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "ResidueTopology.h"
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

ResidueTopology::ResidueTopology() {
  //
  // add EMPTY types to refer to not existed types
  //
  AtomType emptyType;
  addAtomType(emptyType);

  BondType emptyBond;
  addBondType(emptyBond);

  AngleType emptyAngle;
  addAngleType(emptyAngle);

  TorsionType emptyTorsion;
  addTorsionType(emptyTorsion);

}

ResidueTopology::~ResidueTopology() {
	// TODO Auto-generated destructor stub
}

void ResidueTopology::addAtomType(const AtomType& type) {
  atom_types.push_back(type);
}

void ResidueTopology::addBondType(const BondType& type) {
  bond_types.push_back(type);
}
void ResidueTopology::addAngleType(const AngleType& type) {
  angle_types.push_back(type);
}

void ResidueTopology::addTorsionType(const TorsionType& type) {
  torsion_types.push_back(type);
}

bool ResidueTopology::addAtomType(const std::string& input)
{
  boost::regex atomline_base(
      "\\s*(?<Type>\\w+)"
      "\\s+(?<Name>"+name_pattern+")"
      "\\s+(?<Mass>"+double_pattern+")"
      "\\s+(?<Charge>"+double_pattern+")"
      "\\s+(?<pType>\\w)"
      "\\s+(?<Sigma>"+double_pattern+")"
      "\\s+(?<Epsilon>"+double_pattern+")"
      "\\s*;?.*");// possible comment afterwards

  boost::regex atomline_residue(
      "\\s+(?<Name>"+name_pattern+")"
      "\\s+(?<Type>\\w+)"
      "\\s+(?<Charge>"+double_pattern+")"
      "\\s+(?<Serial>\\d+)"
      "\\s*;?.*");// possible comment afterwards

  boost::smatch what;
  if (boost::regex_match(input,what,atomline_base) )
    {

      AtomType at;
      at.setAmberName(what["Type"]);
      at.setName(what["Name"]);
      at.setMass(boost::lexical_cast<double>(what["Mass"]));
      at.setCharge(boost::lexical_cast<double>(what["Charge"]));
      at.setPType(what["pType"]);
      at.setSigma(boost::lexical_cast<double>(what["Sigma"]));
      at.setEpsilon(boost::lexical_cast<double>(what["Epsilon"]));
      addAtomType(at);
      return true;
    }else
  if (boost::regex_match(input,what,atomline_residue) )
  {

    AtomType at;
    at.setName(what["Name"]);
    at.setAmberName(what["Type"]);
    at.setCharge(boost::lexical_cast<double>(what["Charge"]));
    at.setSerial(boost::lexical_cast<int>(what["Serial"]));
    addAtomType(at);
    return true;
  }else
  {
    return false;
  }


}

bool ResidueTopology::addBondType(const std::string& input)
{

  boost::regex bondline_base(
        "\\s+(?<NameI>"+name_pattern+")"
        "\\s+(?<NameJ>"+name_pattern+")"
        "\\s+(?<Func>\\d+)"
        "\\s+(?<B0>"+double_pattern+")"
        "\\s+(?<Kb>"+double_pattern+")"
        "\\s*;?.*");

  boost::regex bondline_residue(
      "\\s+(?<NameI>"+name_pattern+")"
      "\\s+(?<NameJ>"+name_pattern+")"
      "\\s*;?.*");
  boost::smatch what;
  if (boost::regex_match(input,what,bondline_base))
  {
    BondType bt;
    bt.setNameI(what["NameI"]);
    bt.setNameJ(what["NameJ"]);
    bt.setFunc(boost::lexical_cast<int>(what["Func"]));
    bt.setB0(boost::lexical_cast<double>(what["B0"]) *  10 ); // nm to Angstrom
    bt.setKb(boost::lexical_cast<double>(what["Kb"]));
    addBondType(bt);
    return true;
  }
  if (boost::regex_match(input,what,bondline_residue))
  {
    BondType bt;
    bt.setNameI(what["NameI"]);
    bt.setNameJ(what["NameJ"]);
    addBondType(bt);
    return true;
  }else
  {
    return false;
  }

}

bool ResidueTopology::addAngleType(const std::string& input) {

    boost::regex angleline_residue(
        "\\s*(?<NameI>"+name_pattern+")"
        "\\s+(?<NameJ>"+name_pattern+")"
        "\\s+(?<NameK>"+name_pattern+")"
        "(\\s+(?<Func>\\d+))?"
        "\\s+(?<Theta0>"+double_pattern+")"
        "\\s+(?<CTheta>"+double_pattern+")"
        "\\s*;?.*");
    boost::smatch what;
    if (boost::regex_match(input,what,angleline_residue))
    {
      AngleType at;
      at.setNameI(what["NameI"]);
      at.setNameJ(what["NameJ"]);
      at.setNameK(what["NameK"]);

      /* There is ambiguous in angle definition:
       *  field `Func` may or may not be present
       * */
      if (what["Func"].length()>0)
      {
        at.setFunc(boost::lexical_cast<int>(what["Func"]));
      }else
      {
        at.setFunc(0);
      }

      at.setTh0(boost::lexical_cast<double>(what["Theta0"]));
      at.setCth(boost::lexical_cast<double>(what["CTheta"]));
      angle_types.push_back(at);
      return true;
    }
    else
    {
    return false;
  }
}

bool ResidueTopology::addTorsionType(const std::string& input) {
  boost::regex torsionline_phase(
      "\\s*(?<NameI>"+name_pattern+")"
      "\\s+(?<NameJ>"+name_pattern+")"
      "\\s+(?<NameK>"+name_pattern+")"
      "\\s+(?<NameL>"+name_pattern+")"
      "(\\s+(?<Func>\\d+))?"
      "\\s+(?<Phase>"+double_pattern+")"
      "\\s+(?<Kd>"+double_pattern+")"
      "\\s+(?<Pn>\\d+)"
      "\\s*;?.*");

  boost::regex torsionline_6c(
      "\\s*(?<NameI>"+name_pattern+")"
      "\\s+(?<NameJ>"+name_pattern+")"
      "\\s+(?<NameK>"+name_pattern+")"
      "\\s+(?<NameL>"+name_pattern+")"
      "(\\s+(?<Func>\\d+))?"
      "\\s+(?<C0>"+double_pattern+")"
      "\\s+(?<C1>"+double_pattern+")"
      "\\s+(?<C2>"+double_pattern+")"
      "\\s+(?<C3>"+double_pattern+")"
      "\\s+(?<C4>"+double_pattern+")"
      "\\s+(?<C5>"+double_pattern+")"
      "\\s*;?.*");
  boost::smatch what;



  if (boost::regex_match(input,what,torsionline_6c))
  {
    TorsionType tt;
    tt.setNameI(what["NameI"]);
    tt.setNameJ(what["NameJ"]);
    tt.setNameK(what["NameK"]);
    tt.setNameL(what["NameL"]);


    if (what["Func"].length()>0)
    {
      tt.setFunc(boost::lexical_cast<int>(what["Func"]));
    }else
    {
      tt.setFunc(3);
    }

    tt.setC(
        boost::lexical_cast<double>(what["C0"]),
        boost::lexical_cast<double>(what["C1"]),
        boost::lexical_cast<double>(what["C2"]),
        boost::lexical_cast<double>(what["C3"]),
        boost::lexical_cast<double>(what["C4"]),
        boost::lexical_cast<double>(what["C5"]));

    tt.setPhase(0.0);
    tt.setKd(0.0);
    tt.setPn(0);

    addTorsionType(tt);
    return true;
  }

  if (boost::regex_match(input,what,torsionline_phase))
    {
      TorsionType tt;
      tt.setNameI(what["NameI"]);
      tt.setNameJ(what["NameJ"]);
      tt.setNameK(what["NameK"]);
      tt.setNameL(what["NameL"]);

      if (what["Func"].length()>0)
      {
        tt.setFunc(boost::lexical_cast<int>(what["Func"]));
      }else
      {
        tt.setFunc(1);
      }

      tt.setPhase(boost::lexical_cast<double>(what["Phase"]));
      tt.setKd(boost::lexical_cast<double>(what["Kd"]));
      tt.setPn(boost::lexical_cast<int>(what["Pn"]));

      tt.setC(0.0,0.0,0.0,0.0,0.0,0.0);

      addTorsionType(tt);
      return true;
    }


  return false;

}

const std::string& ResidueTopology::getName() const {
  return name;
}

void ResidueTopology::setName(const std::string& name) {
  this->name = name;
}

void ResidueTopology::printAtomTypes(std::ostream& out) const {

  out << "\tATOMS(" << atom_types.size() << "): " << std::endl;
  out << "\t\t";
  out << "name" << "\t";
  out << "AmberName" << "\t";
  out << "Mass" << "\t";
  out << "Charge" << "\t";
  out << "Type" << "\t";
  out << "Sigma" << "\t";
  out << "Epsilon" << "\t";
  out << "Serial" << "\t";
  out << std::endl;
  for (auto it = atom_types.begin(); it != atom_types.end(); ++it) {
    out << "\t\t";
    out << it->getName() << "\t";
    out << it->getAmberName() << "\t";
    out << it->getMass() << "\t";
    out << it->getCharge() << "\t";
    out << it->getPType() << "\t";
    out << it->getSigma() << "\t";
    out << it->getEpsilon() << "\t";
    out << it->getSerial() << "\t";
    out << std::endl;
  }
}

void ResidueTopology::printBondTypes(std::ostream& out) const {
  out << "\tBONDS(" << bond_types.size() << "): " << std::endl;
  for (auto it = bond_types.begin(); it != bond_types.end(); ++it) {
    out << "\t\t";
    out << it->getNameI() << "\t";
    out << it->getNameJ() << "\t";
    out << it->getFunc() << "\t";
    out << it->getB0() << "\t";
    out << it->getKb() << "\t";
    out << std::endl;
  }
}

AtomType ResidueTopology::getAtomType(const std::string& name) const
{
  for (auto it = atom_types.begin();it!=atom_types.end();++it)
  {
    if (it->getName()==name)
    {
      return *it;
    }
  }
  std::cerr<< "ERROR: no atom '"<<name<<"' in residue " << getName()<< std::endl;
  return AtomType();
}
AtomType ResidueTopology::getAtomTypeByAmberName(const std::string& name) const
{
  for (auto it = atom_types.begin();it!=atom_types.end();++it)
  {
    if (it->getAmberName()==name)
    {
      return *it;
    }
  }
  std::cerr<< "ERROR: no atom amber name '"<<name<<"' in residue " << getName()<< std::endl;
  return AtomType();
}


void ResidueTopology::printAngleTypes(std::ostream& out) const {
  out << "\tANGLES(" << angle_types.size() << "): " << std::endl;
  for (auto it = angle_types.begin(); it != angle_types.end(); ++it) {
    out << "\t\t";
    out << it->getNameI() << "\t";
    out << it->getNameJ() << "\t";
    out << it->getNameK() << "\t";
    out << it->getFunc() << "\t";
    out << it->getTh0() << "\t";
    out << it->getCth() << "\t";
    out << std::endl;
  }
}

const std::vector<BondType>& ResidueTopology::getBondTypes() const {
  return bond_types;
}

void ResidueTopology::printTorsions(std::ostream& out) const {
  out << "\tTORSIONS(" << torsion_types.size() << "): " << std::endl;
  for (auto it = torsion_types.begin(); it != torsion_types.end(); ++it) {
    out << "\t\t";
    out << it->getNameI() << "\t";
    out << it->getNameJ() << "\t";
    out << it->getNameK() << "\t";
    out << it->getNameL() << "\t";
    out << it->getFunc() << "\t";
    out << it->getPhase() << "\t";
    out << it->getKd() << "\t";
    out << it->getPn() << "\t";
    out << std::endl;
  }
}

void ResidueTopology::print(std::ostream& out) const {

  printAtomTypes(out);
  printBondTypes(out);
  printAngleTypes(out);
  printTorsions(out);
}

const std::string ResidueTopology::double_pattern("(\\-?\\d*.\\d+)([Ee](\\+|\\-)?\\d+)?");
const std::string ResidueTopology::name_pattern("(\\-|\\+)?[\\w]+");

const std::vector<AtomType>& ResidueTopology::getAtomTypes() const {
  return atom_types;
}

void ResidueTopology::setBondTypes(
    const std::map<std::string, int > & name_to_typeID) {
// BONDS TYPES
  for (auto it = bond_types.begin();it!=bond_types.end();++it)
  {
    if (name_to_typeID.count(it->getNameI())==0)
    {
      std::cerr << " NO atom name " <<it->getNameI() << std::endl;
      continue;
    }
    if (name_to_typeID.count(it->getNameJ())==0)
    {
      std::cerr << " NO atom name " <<it->getNameJ() << std::endl;
      continue;
    }
    it->setTypeI(name_to_typeID.at(it->getNameI()));
    it->setTypeJ(name_to_typeID.at(it->getNameJ()));
  }
// ANGLE TYPES
  for (auto it = angle_types.begin();it!=angle_types.end();++it)
  {
    if (name_to_typeID.count(it->getNameI())==0)
    {
      std::cerr << " NO atom name " <<it->getNameI() << std::endl;
      continue;
    }
    if (name_to_typeID.count(it->getNameJ())==0)
    {
      std::cerr << " NO atom name " <<it->getNameJ() << std::endl;
      continue;
    }
    if (name_to_typeID.count(it->getNameK())==0)
    {
      std::cerr << " NO atom name " <<it->getNameJ() << std::endl;
      continue;
    }
    it->setTypeI(name_to_typeID.at(it->getNameI()));
    it->setTypeJ(name_to_typeID.at(it->getNameJ()));
    it->setTypeK(name_to_typeID.at(it->getNameK()));
  }
// TORSION ANGLES
  for (auto it = torsion_types.begin();it!=torsion_types.end();++it)
   {
     if (name_to_typeID.count(it->getNameI())==0)
     {
       std::cerr << " NO atom name " <<it->getNameI() << std::endl;
       continue;
     }
     if (name_to_typeID.count(it->getNameJ())==0)
     {
       std::cerr << " NO atom name " <<it->getNameJ() << std::endl;
       continue;
     }
     if (name_to_typeID.count(it->getNameK())==0)
     {
       std::cerr << " NO atom name " <<it->getNameJ() << std::endl;
       continue;
     }
     if (name_to_typeID.count(it->getNameL())==0)
     {
       std::cerr << " NO atom name " <<it->getNameL() << std::endl;
       continue;
     }
     it->setTypeI(name_to_typeID.at(it->getNameI()));
     it->setTypeJ(name_to_typeID.at(it->getNameJ()));
     it->setTypeK(name_to_typeID.at(it->getNameK()));
     it->setTypeL(name_to_typeID.at(it->getNameL()));
   }
}

const std::vector<AngleType>& ResidueTopology::getAngleTypes() const {
  return angle_types;
}

const std::vector<TorsionType>& ResidueTopology::getTorsionTypes() const {
  return torsion_types;
}
