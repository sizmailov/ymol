/*
 * Atom.h
 *
 *  Created on: 30 May 2014
 *      Author: sergei
 */

#ifndef ATOM_H_
#define ATOM_H_

#include "../geom/Point.h"
#include "AtomType.h"
//enum class Hybridisation
//{
//	sp2,
//	sp3
//};
//






class Atom {

	int serial;
	int type;

	double charge;

  std::string residue_name;
  int  residue_serial;
  std::string chain_id;
  std::string name;

public:


	Point r;


	Atom();
	Atom(const Point &pos, int atom_type);

	virtual ~Atom();

	  int getSerial() const;
    void setSerial(int serial);

    int getType() const;
    void setType(int type);

    const std::string& getChainId() const;
    void setChainId(const std::string& chainId);

    const std::string& getName() const;
    void setName(const std::string& name);

    const std::string& getResidueName() const;
    void setResidueName(const std::string& residueName);

    int getResidueSerial() const;
    void setResidueSerial(int residueSerial);

    double getCharge() const;
    void setCharge(double charge);
};

#endif /* ATOM_H_ */
