/*
 * AtomType.cpp
 *
 *  Created on: 31 May 2014
 *      Author: sergei
 */

#include "AtomType.h"



AtomType::AtomType():
  name(""),
  amber_name(""),
  mass(0.0),
  charge(0.0),
  pType(""),
  sigma(0.0),
  epsilon(0.0),
  serial(0)
{
}

AtomType::AtomType(std::string name, std::string bond_type, double mass,
    double charge, std::string pType, double sigma, double epsion, int serial):
    name(name),
    amber_name(bond_type),
    mass(mass),
    charge(charge),
    pType(pType),
    sigma(sigma),
    epsilon(epsilon),
    serial(0){

}

const std::string& AtomType::getAmberName() const {
  return amber_name;
}

void AtomType::setAmberName(const std::string& bondType) {
  amber_name = bondType;
}

double AtomType::getCharge() const {
  return charge;
}

void AtomType::setCharge(double charge) {
  this->charge = charge;
}

double AtomType::getEpsilon() const {
  return epsilon;
}

void AtomType::setEpsilon(double epsilon) {
  this->epsilon = epsilon;
}

double AtomType::getMass() const {
  return mass;
}

void AtomType::setMass(double mass) {
  this->mass = mass;
}

const std::string& AtomType::getName() const {
  return name;
}

void AtomType::setName(const std::string& name) {
  this->name = name;
}

const std::string& AtomType::getPType() const {
  return pType;
}

void AtomType::setPType(const std::string& type) {
  pType = type;
}

double AtomType::getSigma() const {
  return sigma;
}

void AtomType::setSigma(double sigma) {
  this->sigma = sigma;
}

AtomType::~AtomType() {
	// TODO Auto-generated destructor stub
}


int AtomType::getSerial() const {
  return serial;
}

void AtomType::setSerial(int serial) {
  this->serial = serial;
}
