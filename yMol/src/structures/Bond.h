/*
 * Bond.h
 *
 *  Created on: 3 Jun 2014
 *      Author: sergei
 */

#ifndef BOND_H_
#define BOND_H_

#include "Atom.h"

class Bond : boost::noncopyable {
    std::shared_ptr<Atom> a;
    std::shared_ptr<Atom> b;

    double b0;
    double kb;
    int func;

  public:

    virtual ~Bond();
    
    std::shared_ptr<Atom> getA() const;
    std::shared_ptr<Atom> getB() const;
    
    double getB0() const;
    double getKb() const;
    int getFunc() const;
    
    
    void setAtoms(std::shared_ptr<Atom> a,
                  std::shared_ptr<Atom> b);
    
    void setB0(double b0);
    void setKb(double kb);
    void setFunc(int func);

    
    static std::shared_ptr<Bond> create();
  private:
    Bond();
};

#endif /* BOND_H_ */
