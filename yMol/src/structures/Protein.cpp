/*
 * Protein.cpp
 *
 *  Created on: 1 Jun 2014
 *      Author: sergei
 */

#include "Protein.h"
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "PotentialParameters.h"

Protein::Protein() {
  // TODO Auto-generated constructor stub

}

void Protein::detectResidues()
{

  for (auto it = atoms.begin();it!=atoms.end();++it)
  {
    bool newRes = false;
    if (residues.size() == 0 )
    {
      newRes = true;
    }
    else
    {
      if (residues.back().getSerial()!= it->getResidueSerial())
      {
        newRes = true;
      }else
      {
        residues.back().addAtom(it);
      }
    }
    if (newRes)
    {
      Residue res;
      res.setName(it->getResidueName());
      res.setSerial(it->getResidueSerial());
      res.addAtom(it);
      residues.push_back(res);
    }

  }
}

void Protein::printResidues(std::ostream & out ) {
  out << "Residues ("<< residues.size() << "):"<< std::endl;
  for (auto it = residues.begin();it!=residues.end();++it)
  {
    AtomSelection atoms = it->getAtoms();
    out << it->getName() << std::endl;
    for (std::size_t i = 0;i<atoms.size();i++)
    {
      out << "\t";
      out << atoms[i]->getSerial() << "\t";
      out << atoms[i]->getName() << "\t";
      out << std::endl;
    }
  }
}

void Protein::setTerminalResidues() {
  auto min = residues.begin();
  auto max = residues.begin();
  for (auto it = residues.begin(); it != residues.end(); ++it) {
    min = min->getSerial() < it->getSerial() ? min : it;
    max = max->getSerial() > it->getSerial() ? max : it;
  }
  min->setName("N" + min->getName());
  max->setName("C" + max->getName());
}

void Protein::correctAtomNames(const PotentialParameters& prm)
{
  setTerminalResidues();
  for (auto it = residues.begin();it!=residues.end();++it)
  {
    ResidueTopology rt = prm.getResidueTopology(it->getName());
    const AtomSelection &as = it->getAtoms();


    for (int i=0;i<as.size();i++){
      std::string name = as[i]->getName();
      if ( it->getName().length()==4 &&  it->getName().substr(0,1)=="C")
      {
        if (name =="OXT")       name = "OC1";
        if (name == "O")       name = "OC2";
      }
      if (name == "HN" ) name = "H";

      if ( it->getName().length()==4 &&  it->getName().substr(0,1)=="N")
      {
        if (name=="HN1")       name = "H1";
        if (name=="HN2")       name = "H2";
        if (name=="HN3")       name = "H3";
      }

      AtomType at = rt.getAtomType(name);
      if (at.getAmberName()==""){
        std::cout << it->getName()     << "\t";
        std::cout << as[i]->getName()  << "\t";
        std::cout << at.getAmberName() << "\t";
        std::cout << std::endl;
      }
      else
      {
        as[i]->setName(name);
        as[i]->setType(prm.getAtomTypeID(at.getAmberName()));
        as[i]->setCharge(at.getCharge());
      }

    }
  }
}




Protein::~Protein() {
  // TODO Auto-generated destructor stub
}


bool readPdbFile(const std::string& filename, Protein & prot)
{
  std::ifstream fin;
  fin.open(filename.c_str(),std::ifstream::in);

  using boost::algorithm::trim_copy;
  if (!fin.good())
  {
    std::cerr << "Could not open file '" << filename<< "'" << std::endl;
    return false;
  }

  while (fin.good())
  {
    std::string line;
    std::getline(fin,line);
    if (line.length()<6) continue;
    if (line.substr(0,6)=="ATOM  "){
      Atom at;
      at.setSerial (boost::lexical_cast<int>(trim_copy(line.substr(6,5))));
      at.setName(trim_copy(line.substr(12,4)));
      at.setResidueName(trim_copy(line.substr(17,3)));

      at.setChainId(trim_copy(line.substr(21,1)));
      at.setResidueSerial(boost::lexical_cast<int>(trim_copy(line.substr(22,4))));

      at.r.x = boost::lexical_cast<double>(trim_copy(line.substr(30,8)));
      at.r.y = boost::lexical_cast<double>(trim_copy(line.substr(38,8)));
      at.r.z = boost::lexical_cast<double>(trim_copy(line.substr(46,8)));
//
//      at.setOccupancy(boost::lexical_cast<double>(trim_copy(line.substr(54,6))));
//      at.setTemperature(boost::lexical_cast<double>(trim_copy(line.substr(60,6))));

      prot.addAtom(at);
    }
  }
  return true;

}

void Protein::setBonds(const PotentialParameters& prm)
{

  int n = atoms.size();

  neigh.resize(n);

  for (int i=0;i<n;i++)
  {
    int typei = atoms[i].getType();
    for (int j =i+1;j<n;j++)
    {
      int typej = atoms[j].getType();

      const BondType &bt = prm.getBondType(typei,typej);
      double b0 = bt.getB0();
      if (b0>0){
        double dr = dist(atoms[i].r,atoms[j].r);

        if (fabs(dr-b0)<0.1)
        {

          neigh[i].push_back(j);
          neigh[j].push_back(i);

          Bond b;
          b.setAtoms(atoms.begin()+i,atoms.begin()+j);
          b.setKb(bt.getKb());
          b.setB0(bt.getB0());
          b.setFunc(bt.getFunc());
          bonds.push_back(b);
        }
      }
    }
  }
}

void Protein::setAngles(const PotentialParameters & prm) {
  int n = atoms.size();
  for (int j=0;j<n;j++)
  {
    for (int t =0;t<neigh[j].size();t++)
    {
      for (int t2 =0;t2<neigh[j].size();t2++)
      {
        int i = neigh[j][t];
        int k = neigh[j][t2];
        if (atoms[i].getSerial() < atoms[k].getSerial())
        {
          Angle angl;
          angl.setAtoms(atoms.begin()+i,atoms.begin()+j,atoms.begin()+k);

          const AngleType &angltype = prm.getAngleType(angl.getA()->getType(),angl.getB()->getType(),angl.getC()->getType());
          angl.setB0(angltype.getTh0());
          angl.setKb(angltype.getCth());
          angl.setFunc(angltype.getFunc());

          angles.push_back(angl);
        }
      }
    }
  }
}

void Protein::printAngles(std::ostream & out ) {
  out << angles.size() << "\t # angles "<< std::endl;
  for (auto it = angles.begin();it!=angles.end();++it)
  {
    out << it->getA()->getSerial()  << "\t";
    out << it->getB()->getSerial()  << "\t";
    out << it->getC()->getSerial()  << "\t";
    out << it->getB0()  << "\t";
    out << it->getKb()  << "\t";

    out << "\t";
    out << "#" <<"\t";

    out << it->getA()->getName()  << "\t";
    out << it->getB()->getName()  << "\t";
    out << it->getC()->getName()  << "\t";
    out << std::endl;

  }

}

void Protein::setTorsions(const PotentialParameters& prm)
{
  int n = atoms.size();


  for (int j=0;j<n;j++)
  {
    for (std::size_t t = 0;t<neigh[j].size();t++)
    {
      int k = neigh[j][t];
      if (j>k) continue;

      for (std::size_t t2=0;t2<neigh[j].size();t2++)
      {
        int i = neigh[j][t2];
        for (std::size_t t3=0;t3<neigh[k].size();t3++)
        {
          int l = neigh[k][t3];
          if (l!=j && i!=k)
          {
            Torsion tor;
            tor.setAtoms(
                atoms.begin()+i,
                atoms.begin()+j,
                atoms.begin()+k,
                atoms.begin()+l);
            const TorsionType &tt = prm.getTorsionType(
                tor.getA()->getType(),
                tor.getB()->getType(),
                tor.getC()->getType(),
                tor.getD()->getType());

            if (!(tt.getNameI()=="" && tt.getNameJ()=="" && tt.getNameK()=="" && tt.getNameL()==""))
            {
              // in case of propper dihedral
              std::cout <<  tor.getA()->getName()<<" ";
              std::cout <<  tor.getB()->getName()<<" ";
              std::cout <<  tor.getC()->getName()<<" ";
              std::cout <<  tor.getD()->getName()<<"  --->  ";
              std::cout << tt.getNameI() << " ";
              std::cout << tt.getNameJ() << " ";
              std::cout << tt.getNameK() << " ";
              std::cout << tt.getNameL() << std::endl;
              tor.setC_(
                  tt.getC0(),
                  tt.getC1(),
                  tt.getC2(),
                  tt.getC3(),
                  tt.getC4(),
                  tt.getC5()
                  );

              // in case of impropper
              tor.setPhase(tt.getPhase());
              tor.setKd(tt.getKd());
              tor.setPn(tt.getPn());

              tor.setFunc(tt.getFunc());
            }
            torsions.push_back(tor);
          }
        }
      }
    }
  }
  std::cout << "  Torsion = " << torsions.size() << std::endl;
}

void Protein::printTorsions(std::ostream& out)
{
  out << torsions.size() << "\t # torsions "<< std::endl;
   for (auto it = torsions.begin();it!=torsions.end();++it)
   {
//     if (it->getB()->getResidueSerial()!=1) continue; // DELETE THIS LINE ;


     out << it->getA()->getSerial()  << "\t";
     out << it->getB()->getSerial()  << "\t";
     out << it->getC()->getSerial()  << "\t";
     out << it->getD()->getSerial()  << "\t";
     if (it->getFunc()==3){
     out << it->getC0()  << "\t";
     out << it->getC1()  << "\t";
     out << it->getC2()  << "\t";
     out << it->getC3()  << "\t";
     out << it->getC4()  << "\t";
     out << it->getC5()  << "\t";
     }
     if (it->getFunc()==1)
     {
       out << it->getPhase()  << "\t";
       out << it->getKd()  << "\t";
       out << it->getPn()  << "\t";
     }

     out << "\t";
     out << "#" <<"\t";

     out << it->getA()->getName()  << "\t";
     out << it->getB()->getName()  << "\t";
     out << it->getC()->getName()  << "\t";
     out << it->getD()->getName()  << "\t";

     out << "#" <<"\t";

     out << it->getA()->getType()  << "\t";
     out << it->getB()->getType()  << "\t";
     out << it->getC()->getType()  << "\t";
     out << it->getD()->getType()  << "\t";
     out << std::endl;

   }

}

void Protein::setSpecificTorsions(const PotentialParameters& prm)
{

  for (auto it = torsions.begin();it!=torsions.end();++it)
  {
    Atom &a = *it->getA();
    Atom &b = *it->getB();
    Atom &c = *it->getC();
    Atom &d = *it->getD();
    std::string aname = a.getName();
    std::string bname = b.getName();
    std::string cname = c.getName();
    std::string dname = d.getName();

    if (a.getResidueSerial()<b.getResidueSerial())
    {
      //      |          |
      //    C + N--CA--C + N
      //      |    :     |
      //      |    CB    |
      //
      // a must be "C" -- > "-C"
      // b must be "N"
      aname = "-C";
    }
    if (b.getResidueSerial()<c.getResidueSerial())
    {
      // b must be "C"
      // c must be "N"  --> "+N"
      // d must be "CA" or "H" --> "+CA" or "+H"
      cname = "+" + cname;
      dname = "+" + dname;
    }
    if (c.getResidueSerial() < d.getResidueSerial() )
    {
      // d must be "N" --> "+N"
      dname = "+" + dname;
    }



    std::string resName = b.getResidueName();


//    std::cout << a.getType() << "\t";
//    std::cout << b.getType() << "\t";
//    std::cout << c.getType() << "\t";
//    std::cout << d.getType() << "\t";
//    std::cout << aname << "\t";
//    std::cout << bname << "\t";
//    std::cout << cname << "\t";
//    std::cout << dname << "\t";
//    std::cout << resName << "\t";
//    std::cout << std::endl;

    TorsionType tt = prm.getSpecificTorsionType(resName,aname,bname,cname,dname);

    if (tt.getNameI()!=""){

      std::cout <<  it->getA()->getName()<<" ";
      std::cout <<  it->getB()->getName()<<" ";
      std::cout <<  it->getC()->getName()<<" ";
      std::cout <<  it->getD()->getName()<<"  --->  ";
      std::cout << tt.getNameI() << " ";
      std::cout << tt.getNameJ() << " ";
      std::cout << tt.getNameK() << " ";
      std::cout << tt.getNameL() << "  (spec)"<<std::endl;

      it->setC_(
           tt.getC0(),
           tt.getC1(),
           tt.getC2(),
           tt.getC3(),
           tt.getC4(),
           tt.getC5()
           );

       // in case of impropper
       it->setPhase(tt.getPhase());
       it->setKd(tt.getKd());
       it->setPn(tt.getPn());

       it->setFunc(tt.getFunc());
    }




  }
}
