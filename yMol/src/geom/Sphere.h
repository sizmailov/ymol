/*
 * Sphere.h
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#ifndef SPHERE_H_
#define SPHERE_H_

#include "Point.h"

class Sphere {
	Point c;
	double r;
public:
	Sphere(const Point & center, double radius);
	virtual ~Sphere();
};

#endif /* SPHERE_H_ */
