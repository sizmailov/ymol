/*
 * Box.cpp
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#include "Box.h"
#include <algorithm>


Box::Box(const Point & lower, double widthX, double widthY, double widthZ)
{
	this->lower = lower;
	upper.x=lower.x + widthX;
	upper.y=lower.y + widthY;
	upper.z=lower.z + widthZ;
}
Box::Box(const Point& vert1, const Point& vert2) {

	lower.x = std::min(vert1.x, vert2.x);
	lower.y = std::min(vert1.y, vert2.y);
	lower.z = std::min(vert1.z, vert2.z);


	upper.x = std::max(vert1.x, vert2.x);
	upper.y = std::max(vert1.y, vert2.y);
	upper.z = std::max(vert1.z, vert2.z);

}

Box::~Box() {
	// TODO Auto-generated destructor stub
}

const Vector Box::getEdgeX() const {
	Vector out;
	out.x = upper.x - lower.x;
	return out;
}

const Vector Box::getEdgeY() const {
	Vector out;
	out.y = upper.y - lower.y;
	return out;
}

const Vector Box::getEdgeZ() const {
	Vector out;
	out.z = upper.z - lower.z;
	return out;
}


const Vector Box::diag() const {
	return range(lower,upper);
}

double Box::widhtX() const {
	return upper.x - lower.x;
}

double Box::widhtY() const {
	return upper.y - lower.y;
}

double Box::widhtZ() const {
	return upper.z - lower.z;
}
