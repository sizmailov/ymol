/*
 * Box.h
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#ifndef BOX_H_
#define BOX_H_

#include "Point.h"

class Box {
	Point lower;
	Point upper;
public:
	Box(const Point & lower, double widthX, double widthY, double widthZ);
	Box(const Point & vet1, const Point & vert2);
	virtual ~Box();

	const Vector getEdgeX() const;
	const Vector getEdgeY() const;
	const Vector getEdgeZ() const;

	const Vector diag() const;

	double widhtX() const;
	double widhtY() const;
	double widhtZ() const;

};

#endif /* BOX_H_ */
