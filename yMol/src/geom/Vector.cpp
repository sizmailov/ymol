/*
 * Vector.cpp
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#include "Vector.h"
#include <boost/format.hpp>
#include <cmath>

Vector::Vector() {
	// TODO Auto-generated constructor stub
	x = y = z  = 0.0;
}

Vector::Vector(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector::~Vector() {
	// TODO Auto-generated destructor stub
}

Vector& Vector::operator +=(const Vector &rhs) {
	this->x += rhs.x;
	this->y += rhs.y;
	this->z += rhs.z;
	return *this;
}

Vector& Vector::operator -=(const Vector &rhs) {
	this->x -= rhs.x;
	this->y -= rhs.y;
	this->z -= rhs.z;
	return *this;
}

Vector& Vector::operator *=(double rhs) {
	this->x *= rhs;
	this->y *= rhs;
	this->z *= rhs;
	return *this;
}

Vector& Vector::operator /=(double rhs) {
	this->x = rhs;
	this->y = rhs;
	this->z = rhs;
	return *this;
}

double Vector::len() const {
	return sqrt(len2());
}

double Vector::len2() const {
	return x*x + y*y + z*z;
}


const Vector outter(const Vector & lhs, const Vector & rhs)
{
	Vector out;
	out.x = lhs.y*rhs.z - lhs.z*rhs.y;
	out.y = lhs.z*rhs.x - lhs.x*rhs.z;
	out.z = lhs.x*rhs.y - lhs.y*rhs.x;
	return out;
}
double inner(const Vector & lhs, const Vector & rhs)
{
	return lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z;
}

std::ostream & operator << (std::ostream & out , const Vector  & rhs)
{
	out << boost::format("%10.5f %10.5f %10.5f")
				% rhs.x
				% rhs.y
				% rhs.z;
	return out;
}

void Vector::rotateAround(const Vector& v, double angle) {

	double x1,y1,z1;
	double vlen = v.len();
	double vx = v.x/vlen;
	double vy = v.y/vlen;
	double vz = v.z/vlen;

	double cs = cos(angle);
	double sn = sin(angle);

	x1 = ( cs + vx*vx*(1.0-cs)   ) * x
	    +( vx*vy*(1.0-cs) - vz*sn) * y
	    +( vx*vz*(1.0-cs) + vy*sn) * z;

	y1 = ( vx*vy*(1.0-cs) + vz*sn) * x
 	    +( cs + vy*vy*(1.0-cs)   ) * y
	    +( vy*vz*(1.0-cs) - vx*sn) * z;

	z1 = ( vx*vz*(1.0-cs) - vy*sn) * x
		+( vy*vz*(1.0-cs) + vx*sn) * y
		+( cs + vz*vz*(1.0-cs)   ) * z;

	x = x1;
	y = y1;
	z = z1;


}

const Vector operator+ (const Vector& lhs, const Vector& rhs) {
	Vector out(lhs);
	out += rhs;
	return out;
}

const Vector operator- (const Vector& lhs, const Vector& rhs) {
	Vector out(lhs);
	out -= rhs;
	return out;
}

const Vector operator* (double lhs, const Vector& rhs) {
	Vector out(rhs);
	out *= lhs;
	return out;
}

const Vector operator*(const Vector& lhs, double rhs) {
	Vector out(lhs);
	out *= rhs;
	return out;

}

const Vector operator/ (const Vector& lhs, double rhs) {
	Vector out(lhs);
	out /= rhs;
	return out;
}
