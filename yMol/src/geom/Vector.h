/*
 * Vector.h
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#ifndef VECTOR_H_
#define VECTOR_H_
#include <ostream>
#include <vector>


class Vector {
public:

	double x,y,z;

	Vector();
	Vector(double x , double y ,  double z );
	~Vector();

	Vector & operator+= (const Vector &rhs);
	Vector & operator-= (const Vector &rhs);
	Vector & operator*= (double rhs);
	Vector & operator/= (double rhs);

	double len() const;
	double len2() const;

	void rotateAround(const Vector &v, double angle);


	friend const Vector outter(const Vector & lhs, const Vector & rhs);
	friend double inner(const Vector & lhs, const Vector & rhs);

	friend std::ostream & operator << (std::ostream & out , const Vector  & rhs);

};

const Vector operator + (const Vector &lhs, const Vector &rhs);
const Vector operator - (const Vector &lhs, const Vector &rhs);
const Vector operator * (double lhs, const Vector &rhs);
const Vector operator * (const Vector &lhs, double rhs);
const Vector operator / (const Vector &lhs, double rhs);




#endif /* VECTOR_H_ */
