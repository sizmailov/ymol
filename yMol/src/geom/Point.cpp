/*
 * Point.cpp
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#include "Point.h"


Point::Point()
{
	this->x = x;
	this->y = y;
	this->z = z;
}
Point::Point(double x, double y, double z){
	this->x = x;
	this->y = y;
	this->z = z;
}


double dist2(const Point & lhs, const Point & rhs)
{
	return range(lhs,rhs).len2();
}
double dist(const Point & lhs, const Point & rhs)
{
	return range(lhs,rhs).len();
}

const Vector range(const Point & lhs, const Point & rhs)
{
	Vector out;
	out.x = rhs.x - lhs.x;
	out.y = rhs.y - lhs.y;
	out.z = rhs.z - lhs.z;
	return out;
}
