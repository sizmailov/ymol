/*
 * Point.h
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */

#ifndef POINT_H_
#define POINT_H_

#include "Vector.h"

class Point {
public:

	double x,y,z;

	Point();
	Point(double x,double y,double z);

};

double dist(const Point & lhs, const Point & rhs);
const Vector range(const Point & lhs, const Point & rhs);

#endif /* POINT_H_ */
