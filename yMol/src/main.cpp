/*
 * main.cpp
 *
 *  Created on: 29 May 2014
 *      Author: sergei
 */


#include "geom/Vector.h"
#include "structures/PotentialParameters.h"
#include <iostream>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include "structures/Protein.h"


int main(int argc, char **argv) {

  std::vector <ResidueTopology > restop;

  std::string input_filename = "/home/sergei/git/yMol/yMol/res/ffamber03/ffamber03.prep";
  PotentialParameters prm(input_filename);
//  prm.print(std::cout);

  std::string pdb_filename = "/home/sergei/git/yMol/yMol/res/met_enk_with_h.pdb";
  Protein prot;
  readPdbFile(pdb_filename,prot);
//  prot.printAtoms(std::cout);

  prot.detectResidues();
//  prot.printResidues(std::cout);
  prot.correctAtomNames(prm);
  prot.setBonds(prm);
  prot.setAngles(prm);
  prot.setTorsions(prm);
  prot.setSpecificTorsions(prm);
//
//  prot.printAtoms(std::cout,prm);
//  prot.printAngles(std::cout);

  prot.printTorsions(std::cout);
  std::cout << "END OF PROGRAM" << std::endl;





	return 0;
}
