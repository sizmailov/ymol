/*
 * Logger.h
 *
 *  Created on: 6 Jun 2014
 *      Author: sergei
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <mutex>
#include <map>
#include <boost/lexical_cast.hpp>

class CLogger
{
public:
    enum LOG_LEVEL
    {
      VERBOSE,
      DEBUG,
      TRACE,
      WARNING,
      _ERROR
    };
        static CLogger& Instance();
    void init(const std::string & filename, LOG_LEVEL log_lvl);
    void Log(const std::string & log_info, LOG_LEVEL log_lvl, int indent_inc=0);
private:
  int indent;
  std::map<LOG_LEVEL, std::string> level_names;
  LOG_LEVEL log_level;
  std::string logfile;
  std::mutex mutex;
  CLogger(){};
  CLogger(const CLogger& root);
  CLogger& operator=(const CLogger&);
};

#ifdef _WIN32
  #define __FUNCTION_SIGNATURE__ __FUNCSIG__
#else
  #define __FUNCTION_SIGNATURE__ __PRETTY_FUNCTION__
#endif


#define LOG_(str , lvl , idnt) (CLogger::Instance().Log((std::string("")+(str)), (lvl), (idnt)))
#define LOG_VERBOSE(str) LOG_((str), (CLogger::VERBOSE),0 )
#define LOG_TRACE(str) LOG_((str), (CLogger::TRACE),0 )
#define LOG_DEBUG(str) LOG_((str), (CLogger::DEBUG),0 )
#define LOG_ERROR(str) LOG_((str), (CLogger::_ERROR), 0 )
#define LOG_WARNING(str) LOG_((str), (CLogger::WARNING),0 )
#define LOG_TRACE_ENTER_FUNCTION() LOG_("Entering function '" + TO_STRING(__FUNCTION_SIGNATURE__)+"' file:" + TO_STRING(__FILE__) , (CLogger::TRACE) , +1 )
#define LOG_TRACE_LEAVE_FUNCTION() LOG_("Leaving function  '" + TO_STRING(__FUNCTION_SIGNATURE__)+"'" , (CLogger::TRACE) , -1 )
#define LOG_DEBUG_ENTER_FUNCTION() LOG_("Entering function '" + TO_STRING(__FUNCTION_SIGNATURE__)+"' file:" + TO_STRING(__FILE__), (CLogger::DEBUG), +1)
#define LOG_DEBUG_LEAVE_FUNCTION() LOG_("Leaving function  '" + TO_STRING(__FUNCTION_SIGNATURE__)+"'", (CLogger::DEBUG) , -1 )
#define LOG_VERBOSE_ENTER_FUNCTION() LOG_("Entering function '" + TO_STRING(__FUNCTION_SIGNATURE__)+"' file:" + TO_STRING(__FILE__), (CLogger::VERBOSE), +1))
#define LOG_VERBOSE_LEAVE_FUNCTION() LOG_("Leaving function  '" + TO_STRING(__FUNCTION_SIGNATURE__)+"'", (CLogger::VERBOSE), -1))
#define LOG_TRACE_ENTER_SCOPE(scopename) LOG_("Entering '" #scopename "' scope; line:"+TO_STRING(__LINE__)+" file:" + TO_STRING(__FILE__), (CLogger::TRACE) , +1 )
#define LOG_TRACE_LEAVE_SCOPE(scopename) LOG_("Leaving  '" #scopename "'", (CLogger::TRACE) , -1 )
#define TO_STRING(param) boost::lexical_cast<std::string>((param))
#endif /* LOGGER_H_ */
